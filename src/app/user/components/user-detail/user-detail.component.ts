import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserApiCallService } from '../../service/user-api-call.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  public userId:number;
  public userInfo:any;

  constructor(
    private route: ActivatedRoute,
    private userApiCall: UserApiCallService,
  ) {}

  ngOnInit() {
    this.userId = this.route.snapshot.params.id;
    this.fetchUser(this.userId)
  }

  fetchUser(user_id:number){
    this.userApiCall.fetchUser(user_id).subscribe(
     (res) => this.userInfo = res.data
    )
  }

}
