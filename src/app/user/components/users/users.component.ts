import { Component, OnInit, ViewChild } from '@angular/core';
import { UserApiCallService } from '../../service/user-api-call.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  public userList: any;
  public totalPages: number;
  public totalPagesList: number[] = [];
  public currentPage: number;

  constructor(
    private userApiCall: UserApiCallService,
    private router: Router
  ) {}

  ngOnInit() {
    this.paginate(1)
    this.currentPage = 1;
  }

  setUser(res) {
    this.userList = res.data;
    if (res.total_pages != this.totalPages) {
      this.createPaginateList(res.total_pages);
    }
    this.totalPages = res.total_pages;
  }

  paginate(pageNum: number) {
    if (pageNum != this.currentPage) {
      this.currentPage = pageNum;
      this.userApiCall
        .fetchUserList(pageNum)
        .subscribe((res) => this.setUser(res));
    }
  }

  createPaginateList(numberOfPages: number) {
    for (let i = 1; i <= numberOfPages; i++) {
      this.totalPagesList.push(i);
    }
  }

  redirect(id: number) {
    this.router.navigate(['users/', id]);
  }
}
