import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './components/users/users.component';
import {UserRouting} from './user-routing.module';
import {UserApiCallService} from './service/user-api-call.service';
import { HttpClientModule } from '@angular/common/http';
import { UserDetailComponent } from './components/user-detail/user-detail.component';

@NgModule({
  declarations: [UsersComponent, UserDetailComponent],
  imports: [
    CommonModule,
    UserRouting,
    HttpClientModule,
    
  ],
  providers: [UserApiCallService],
})
export class UserModule { }
