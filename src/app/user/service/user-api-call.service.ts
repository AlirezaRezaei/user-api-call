import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserApiCallService {
  public userList: any;
  private baseUrl: string = 'https://reqres.in/api/';
  constructor(private http: HttpClient) {}

  fetchUserList(page_number: number): Observable<any> {
    let url = `${this.baseUrl}users?page=${page_number}&per_page=5`;
    return this.http.get(url);
  }
  fetchUser(user_id: number): Observable<any> {
    let url = `${this.baseUrl}users/${user_id}`;
    return this.http.get(url);
  }
}
