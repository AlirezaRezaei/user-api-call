import { TestBed } from '@angular/core/testing';

import { UserApiCallService } from './user-api-call.service';

describe('UserApiCallService', () => {
  let service: UserApiCallService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserApiCallService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
